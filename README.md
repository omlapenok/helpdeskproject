![](https://previews.123rf.com/images/artsterdam/artsterdam1803/artsterdam180300067/98339434-plantilla-de-logotipo-del-servicio-de-asistencia-servicio-de-soporte-de-dise%C3%B1o-vectorial-logotipo-de.jpg)
## Клиент-серверное приложение
Наше приложение запускается по ссылке [http://localhost:9999/](http://localhost:9999/
)

### Студенты:
- 🐱‍🏍**Лапенок Ольга** (https://bitbucket.org/omlapenok/)
- 🐱‍💻**Малышев Сергей**(https://bitbucket.org/malyshev-sergey/)
- 🐱‍👤**Микишев Артем** (https://bitbucket.org/artemmikishev/)
- 🐱‍👓**Садиков Александр** (https://bitbucket.org/alsadi1982/)

## О проекте

Helpdesk – это система, предназначенная для автоматизации обработки запросов клиентов на техническое обслуживание.  
Благодаря внедрению системы Helpdesk, пользователи получают возможность оперативно создавать обращения в службу техподдержки, а сотрудники техподдержки решать неисправности, запрашивать у пользователей дополнительную информацию о проблеме.  

### Основные роли и их возможности  

Admin:  

- Назначает роль
- Блокирует пользователя
- Изменяет данные пользователя
- Устанавливает категории обращений  
 
Client:  

- Создание заявки на обслуживание
- Просмотр списка всех своих обращений
- Просмотр всей информации по конкретному обращению
- Добавление дополнительной информации по заявке  
 
Support:  

- Взятие заявки в работу
- Просмотр списка всех заявок, находящихся в работе
- Просмотр всей информации и переписки по конкретной заявке
- Отправить ответ по заявке
- Изменить статус заявки  

***
## Техническая документация  
> Для начала работы необходимо:
>> mvn clean package  
>> cp target/helpdesk-1.0.0.jar devops  
>> cd devops  
>> docker-compose build  
>> docker-compose up  
>> [http://localhost:9999/](http://localhost:9999/)

***
   
## Учетные записи 

>Администратора:  
>>login: admin  
>>password: admin  

>Сотрудника поддержки:  
>>login: support  
>>password: support  

>Пользователя:
>>login: user1  
>>password: user1  

## Использованные технологии

- **Spring Boot**
- **Spring Security**
- **Spring AOP**
- **Spring Data**
- **Apache Tomcat**
- **Rest API**
- **HTML**
- **CSS**
- **Thymleaf**
- **PostgreSQL**
- **Log4j**
- **Sonarqube**
- **Docker**