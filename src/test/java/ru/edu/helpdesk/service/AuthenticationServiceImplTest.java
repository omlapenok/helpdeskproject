package ru.edu.helpdesk.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.entity.UserRole;
import ru.edu.helpdesk.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

/**
 * AdminServiceImpl Test
 */
@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceImplTest {

    @InjectMocks
    private AuthenticationServiceImpl authenticationService;

    @Mock
    private UserRepository userRepository;


    private final User ADMIN = new User(1L, "admin", "admin", "admin", "admin", UserRole.ADMIN);

    /**
     * Проверяем успешное выполнеие метода AuthenticationServiceImpl#saveUser(User user)
     */
    @Test
    public void SaveUser_Test() {
        authenticationService.saveUser(ADMIN);
        verify(userRepository, times(1)).save(ADMIN);
    }

    /**
     * Проверяем успешное выполнеие метода AuthenticationServiceImpl#getUserByUsername(String userName)
     */
    @Test
    public void getUserByUsername_Successful_Test() {
        when(userRepository.findByLogin("valera")).thenReturn(ADMIN);

        assertEquals(ADMIN, authenticationService.getUserByUsername("valera"));
    }

    /**
     * Проверяем негативный сценарий выполнения метода AuthenticationServiceImpl#getUserByUsername(String userName)
     */
    @Test
    public void getUserByUsername_Fail_Test() {
        when(userRepository.findByLogin("null")).thenReturn(null);

        assertNull(authenticationService.getUserByUsername("null"));
    }
}