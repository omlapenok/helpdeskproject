package ru.edu.helpdesk.service;

import ru.edu.helpdesk.entity.Ticket;
import ru.edu.helpdesk.entity.Title;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.entity.UserRole;

import java.util.List;

public interface TicketService {

    /**
     * Создание ticket
     *
     * @param ticket
     */
    void createTicket(Ticket ticket);

    /**
     * Просмотр инфо по ticket
     *
     * @param id
     * @return Ticket
     */
    Ticket ticketInfo(long id);

    /**
     * найти все ticket по id клиента
     */
    List<Ticket> allTicketsByClientId(long clientId);

    /**
     * найти все title
     */
    List<Title> allTitles();

    /**
     * Поиск юзера по id
     */
    UserRole getRole(User user);
}
