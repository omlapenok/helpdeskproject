package ru.edu.helpdesk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.edu.helpdesk.entity.Title;

import java.util.List;

/**
 * Интерфейс репозитория для работы с категориями
 */
@Repository
public interface TitleRepository extends JpaRepository<Title, Long> {
    public List<Title> findAllByMarkFalse();

    public List<Title> findAllByOrderByNameAsc();
}